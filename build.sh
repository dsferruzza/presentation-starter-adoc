#!/usr/bin/env bash

# Exit if something fails
set -e

# Options
OUT="build"
NM="node_modules"

echo "-- Prepare build directory"
if [ -d "$OUT" ]; then
  rm -r "${OUT:?}/"
fi
mkdir -p $OUT/

echo "-- Build theme"
./$NM/.bin/node-sass --source-map $OUT/css/theme/theme.css.map --source-map-contents --output-style compressed src/theme.scss $OUT/css/theme/theme.css

echo "-- Copy fonts"
mkdir -p $OUT/fonts/lato
mkdir -p $OUT/fonts/roboto-slab
mkdir -p $OUT/fonts/source-code-pro
mkdir -p $OUT/fonts/font-awesome
cp -r $NM/lato-font/fonts/{lato-heavy,lato-heavy-italic,lato-normal,lato-normal-italic} $OUT/fonts/lato/
cp -r $NM/roboto-slab-fontface-kit/fonts/Bold $OUT/fonts/roboto-slab/
cp -r $NM/source-code-pro/{WOFF2/TTF/SourceCodePro-Regular.ttf.woff2,WOFF/OTF/SourceCodePro-Regular.otf.woff,OTF/SourceCodePro-Regular.otf,TTF/SourceCodePro-Regular.ttf} $OUT/fonts/source-code-pro/
cp -r $NM/@fortawesome/fontawesome-free/webfonts/{fa-regular*,fa-solid*} $OUT/fonts/font-awesome/

echo "-- Import Reveal.js"
mkdir -p $OUT/css
./$NM/.bin/node-sass --source-map $OUT/css/reveal.css.map --source-map-contents --output-style compressed $NM/reveal.js/css/reveal.scss $OUT/css/reveal.css
cp -r $NM/reveal.js/css/print/ $OUT/css/
mkdir -p $OUT/js
cp $NM/reveal.js/js/reveal.js $OUT/js*
mkdir -p $OUT/lib/js
cp $NM/reveal.js/lib/js/html5shiv.js $OUT/lib/js/
touch $OUT/lib/js/head.min.js $OUT/lib/js/classList.js
mkdir -p $OUT/plugin
cp -r $NM/reveal.js/plugin/{highlight,zoom-js,notes,print-pdf} $OUT/plugin/

echo "-- Bundle build script"
./$NM/.bin/browserify src/build.js --transform brfs --outfile $OUT/build.js --node --ignore "fsevents"

echo "-- Copy default content"
cp src/*.adoc $OUT/

echo "-- Build default content"
node $OUT/build.js build --input $OUT/presentation.adoc --output $OUT/index.html

echo "-- Copy other files"
cp .editorconfig $OUT/
cp src/.gitlab-ci.yml $OUT/
cp src/.gitignore $OUT/
cp src/buildDocuments.sh $OUT/

echo "-- Done!"
