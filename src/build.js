const chokidar = require("chokidar");
const cliparse = require("cliparse");
const fs = require("fs");
const path = require("path");

const asciidoctor = require("asciidoctor.js")();
const asciidoctorRevealjs = require("asciidoctor-reveal.js");
asciidoctorRevealjs.register();

function fromEntries(iterable) {
  return [...iterable].reduce((obj, { 0: key, 1: val }) => Object.assign(obj, { [key]: val }), {});
}

const slidesOptions = [
  cliparse.option("input", {
    aliases: [ "i", "in" ],
    parser: cliparse.parsers.existingPathParser,
    description: "The input AsciiDoc document",
    default: "presentation.adoc",
  }),
  cliparse.option("output", {
    aliases: [ "o", "out" ],
    parser: cliparse.parsers.stringParser,
    description: "The Reveal.js slides to output",
    default: "index.html",
  }),
];

const documentOptions = [
  cliparse.option("input", {
    aliases: [ "i", "in" ],
    parser: cliparse.parsers.existingPathParser,
    description: "The input AsciiDoc document",
    default: "presentation.adoc",
  }),
  cliparse.option("output", {
    aliases: [ "o", "out" ],
    parser: cliparse.parsers.stringParser,
    description: "The HTML document to output",
    default: "presentation.html",
  }),
  cliparse.option("use-attributes-from", {
    aliases: [ "config" ],
    parser: cliparse.parsers.existingPathParser,
    description: "An AsciiDoctor documents which attributes should be used to do the conversion",
    default: "presentation.adoc",
  }),
  cliparse.option("attributes-to-import", {
    aliases: [ "a", "attrs" ],
    parser: cliparse.parsers.stringParser,
    description: "Comma-separated list of attributes to import",
    default: "",
  }),
];

const cliParser = cliparse.cli({
  name: "node build.js",
  description: "CLI tool to build Reveal.js slides from an AsciiDoc document",
  commands: [
    cliparse.command("build", { description: "Build the slides once", options: slidesOptions }, buildOnce),
    cliparse.command("watch", { description: "Build the slides everytime the AsciiDoc documents change", options: slidesOptions }, watch),
    cliparse.command("doc", { description: "Build the slides or parts of the slides as a HTML document", options: documentOptions }, buildDocument),
  ],
});

function buildOnce(params) {
  const input = path.resolve(params.options.input);
  const output = path.resolve(params.options.output);
  console.info(`Building '${input}' to '${output}'`);

  const options = {
    safe: "safe",
    backend: "revealjs",
    to_file: false,
    header_footer: true,
    attributes: {
      revealjsdir: ".",
      revealjs_theme: "theme",
      "highlightjs-theme": "#",
      revealjs_slideNumber: true,
      revealjs_history: true,
      revealjs_transition: "none",
      "source-highlighter": "highlightjs",
      icons: "font",
      "iconfont-remote": false,
      "iconfont-name": "#",
      "sectids!": "",
      "revealjs_defaultTiming": null, // Useless because of a bug
    },
  };

  // I have to modify the output manually because there is a bug inside asciidoctor(-reveal).js
  // that makes the "revealjs_defaultTiming" attribute become "revealjs_defaulttiming"...
  const rawOutput = asciidoctor.convertFile(input, options);
  fs.writeFileSync(output, rawOutput.replace("defaultTiming: 120", "defaultTiming: null"));
}

function watch(params) {
  buildOnce(params);

  const inputDir = path.dirname(params.options.input);
  console.info(`Watching changes of AsciiDoc files in ${inputDir}`);

  chokidar.watch("**/*.adoc", {
    useFsEvents: false,
    ignoreInitial: true,
  }).on("all", (event, path) => {
    if (["add", "change", "unlink"].includes(event)) {
      console.info(`A '${event}' event on '${path}' triggered a rebuild`);
      buildOnce(params);
    }
  });
}

function buildDocument(params) {
  const input = path.resolve(params.options.input);
  const output = path.resolve(params.options.output);
  const attrsFile = path.resolve(params.options["use-attributes-from"]);
  const attrsToImport = params.options["attributes-to-import"].split(",").map(a => a.trim());
  console.info(`Building '${input}' to '${output}' using attributes '${attrsToImport.join(", ")}' from '${attrsFile}')`);

  const attrsDocument = asciidoctor.loadFile(attrsFile);
  const importedAttributes = attrsDocument.getAttributes();

  const asciidoctorCss = fs.readFileSync("node_modules/asciidoctor.js/dist/css/asciidoctor.css", "utf8");
  const cssFilename = "asciidoctor.css";
  const cssPath = path.resolve(path.dirname(input), cssFilename);
  fs.writeFileSync(cssPath, asciidoctorCss);

  const importedAttrsValues = fromEntries(attrsToImport.reduce((acc, cur) => acc.set(cur, importedAttributes[cur]), new Map()));
  const attributes = Object.assign(importedAttrsValues, {
    "reproducible": "",
    "stylesheet": cssFilename,
    "source-highlighter": "highlightjs",
    icons: "font",
    "data-uri": "",
    "imagesdir": path.resolve(importedAttributes.imagesdir),
    "toc": attrsDocument.hasAttribute("toc") ? "" : null,
    "toc-position": attrsDocument.getAttribute("toc-position"),
    "toc-title": asciidoctor.loadFile(input).getTitle(),
  });
  const options = {
    safe: "unsafe",
    backend: "html",
    to_file: output,
    header_footer: true,
    attributes,
  };

  asciidoctor.convertFile(input, options);
  fs.unlinkSync(cssPath);
}

cliparse.parse(cliParser);
